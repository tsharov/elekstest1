using com.tsharov.EleksTest1.ApplicationLayer.Services;
using com.tsharov.EleksTest1.ApplicationLayer.ViewModel;
using com.tsharov.EleksTest1.Infrastructure;
using com.tsharov.EleksTest1.Infrastructure.Exceptions;
using MediatR;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using Xunit;

namespace com.tsharov.EleksTest1.UnitTests.Application
{
    public class CustomerAppServiceTest
    {

        private readonly Mock<PaymentContext> _context;
        private readonly Mock<ILogger<CustomerAppService>> _loggerMock;

        public CustomerAppServiceTest()
        {
            _context = new Mock<PaymentContext>();
            _loggerMock = new Mock<ILogger<CustomerAppService>>();
        }


        [Fact]
        public async System.Threading.Tasks.Task Service_throws_exception_when_no_request_data()
        {
            // Arrange

            // Act
            var service = new CustomerAppService(_loggerMock.Object, _context.Object);

            // Assert
            var exception = await Assert.ThrowsAsync<CustomerDomainException>(() => service.FindCustomer(null));
            Assert.Equal("No inquiry criteria", exception.Message);
        }

        [Fact]
        public async System.Threading.Tasks.Task Service_throws_exception_when_empty_request_data()
        {
            // Arrange
            var requestViewModel = new CustomerRequestViewModel();

            // Act
            var service = new CustomerAppService(_loggerMock.Object, _context.Object);

            // Assert
            var exception = await Assert.ThrowsAsync<CustomerDomainException>(() => service.FindCustomer(requestViewModel));
            Assert.Equal("No inquiry criteria", exception.Message);
        }

        [Fact]
        public async System.Threading.Tasks.Task Service_throws_exception_when_invalid_customer_id()
        {
            // Arrange
            var requestViewModel = new CustomerRequestViewModel()
            {
                CustomerId = -123
            };

            // Act
            var service = new CustomerAppService(_loggerMock.Object, _context.Object);

            // Assert
            var exception = await Assert.ThrowsAsync<CustomerDomainException>(() => service.FindCustomer(requestViewModel));
            Assert.Equal("Invalid Customer ID", exception.Message);
        }

        [Fact]
        public async System.Threading.Tasks.Task Service_throws_exception_when_invalid_email()
        {
            // Arrange
            var requestViewModel = new CustomerRequestViewModel()
            {
                Email = "abcdefgijkml"
            };

            // Act
            var service = new CustomerAppService(_loggerMock.Object, _context.Object);

            // Assert
            var exception = await Assert.ThrowsAsync<CustomerDomainException>(() => service.FindCustomer(requestViewModel));
            Assert.Equal("Invalid Email", exception.Message);
        }
    }
}
