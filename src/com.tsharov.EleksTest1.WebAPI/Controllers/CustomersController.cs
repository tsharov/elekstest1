﻿using com.tsharov.EleksTest1.ApplicationLayer.Services;
using com.tsharov.EleksTest1.ApplicationLayer.ViewModel;
using com.tsharov.EleksTest1.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ILogger<CustomersController> _logger;
        private readonly CustomerAppService _customerAppService;

        public CustomersController(ILogger<CustomersController> logger, CustomerAppService customerAppService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _customerAppService = customerAppService ?? throw new ArgumentNullException(nameof(customerAppService));
        }

        // GET api/v1/[controller][?customerId=123456&email=user@domain.com]
        [HttpGet]
        [ProducesResponseType(typeof(CustomerResponseViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Find([FromQuery]CustomerRequestViewModel requestViewModel)
        {
            try
            {
                var model = await _customerAppService.FindCustomer(requestViewModel);

                if (model != null)
                {
                    return Ok(model);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (CustomerDomainException exception)
            {
                return BadRequest(exception.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}