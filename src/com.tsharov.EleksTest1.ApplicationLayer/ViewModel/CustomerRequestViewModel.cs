﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.ApplicationLayer.ViewModel
{
    public class CustomerRequestViewModel
    {
        public long? CustomerId { get; set; }
        public string Email { get; set; }
    }
}
