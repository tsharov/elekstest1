﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.ApplicationLayer.ViewModel
{
    public class CustomerResponseViewModel
    {
        public long CustomerID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

        public List<CustomerTransactionResponseViewModel> Transactions { get; set;}

        public CustomerResponseViewModel(Customer customer, IEnumerable<Transaction> transactions)
        {
            this.CustomerID = customer.Id;
            this.Name = customer.Name;
            this.Email = customer.Email;
            this.Mobile = customer.Mobile;

            this.Transactions = transactions.Select(t => new CustomerTransactionResponseViewModel(t)).ToList();
        }
    }
}
