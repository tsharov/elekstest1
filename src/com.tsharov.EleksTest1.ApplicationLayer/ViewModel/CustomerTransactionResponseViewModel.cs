﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace com.tsharov.EleksTest1.ApplicationLayer.ViewModel
{
    public class CustomerTransactionResponseViewModel
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Status { get; set; }

        public CustomerTransactionResponseViewModel(Transaction transaction)
        {
            this.Id = transaction.Id;
            this.Date = transaction.Time;
            this.Amount = transaction.Amount;
            this.Currency = transaction.CurrencyCode.Name;
            this.Status = transaction.Status.Name;
        }
    }
}
