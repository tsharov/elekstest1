﻿using com.tsharov.EleksTest1.Infrastructure;
using com.tsharov.EleksTest1.Infrastructure.Exceptions;
using com.tsharov.EleksTest1.ApplicationLayer.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.ApplicationLayer.Services
{
    public class CustomerAppService
    {
        private readonly ILogger<CustomerAppService> _logger;
        private readonly PaymentContext _paymentContext;

        public CustomerAppService(ILogger<CustomerAppService> logger, PaymentContext context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _paymentContext = context ?? throw new ArgumentNullException(nameof(context));

            //context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<CustomerResponseViewModel> FindCustomer(CustomerRequestViewModel requestViewModel)
        {

            if (requestViewModel == null)
            {
                throw new CustomerDomainException("No inquiry criteria");
            }

            var customerId = requestViewModel.CustomerId;
            var email = requestViewModel.Email;
            if ((!customerId.HasValue && string.IsNullOrEmpty(email)))
            {
                throw new CustomerDomainException("No inquiry criteria");
            }
            if (customerId.HasValue && customerId.Value < 1)
            {
                throw new CustomerDomainException("Invalid Customer ID");
            }
            if (!string.IsNullOrEmpty(email) && !new EmailAddressAttribute().IsValid(email))
            {
                throw new CustomerDomainException("Invalid Email");
            }

            try
            {
                var data = await _paymentContext.Customers.Where(ci =>
                        (!customerId.HasValue || customerId.Value == ci.Id)
                            && (string.IsNullOrEmpty(email) || email.ToLower() == ci.Email.ToLower())
                    )
                    .Include(c => c.Transactions)
                        .ThenInclude(t => t.CurrencyCode)
                    .Include(c => c.Transactions)
                        .ThenInclude(t => t.Status)
                    .Select(c => new { Customer = c, Transactions = c.Transactions.Take(5) })
                    .FirstOrDefaultAsync();

                return data != null ? new CustomerResponseViewModel(data.Customer, data.Transactions) : null;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
