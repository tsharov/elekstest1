﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.BusinessLayer.Model
{
    /// <remarks> 
    /// CurrencyCode class should be marked as abstract with protected constructor to encapsulate known enum types
    /// this is currently not possible as EleksTest1ContextSeed uses this constructor to load currencyCodes from csv file
    /// </remarks>
    public class CurrencyCode : Enumeration
    {
        public static CurrencyCode USD = new CurrencyCode(1, "USD");
        public static CurrencyCode JPY = new CurrencyCode(2, "JPY");
        public static CurrencyCode THB = new CurrencyCode(3, "THB");
        public static CurrencyCode SGD = new CurrencyCode(4, "SGD");
        public static CurrencyCode UAH = new CurrencyCode(5, "UAH");

        public CurrencyCode(int id, string name)
            : base(id, name)
        {
        }
    }
}
