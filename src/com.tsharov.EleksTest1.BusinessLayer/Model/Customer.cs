﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.BusinessLayer.Model
{
    public class Customer
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public List<Transaction> Transactions { get; set; }

        public Customer()
        {
            Transactions = new List<Transaction>();
        }
    }
}
