﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.BusinessLayer.Model
{
    /// <remarks> 
    /// TransactionStatus class should be marked as abstract with protected constructor to encapsulate known enum types
    /// this is currently not possible as EleksTest1ContextSeed uses this constructor to load transactionStatuses from csv file
    /// </remarks>
    public class TransactionStatus : Enumeration
    {
        public static TransactionStatus Success = new TransactionStatus(1, "Success");
        public static TransactionStatus Failed = new TransactionStatus(2, "Failed");
        public static TransactionStatus Cancelled = new TransactionStatus(3, "Cancelled");

        public TransactionStatus(int id, string name)
            : base(id, name)
        {
        }
    }

}
