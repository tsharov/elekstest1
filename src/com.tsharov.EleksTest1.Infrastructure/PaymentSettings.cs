﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.Infrastructure
{
    public class PaymentSettings
    {
        public bool UseCustomizationData { get; set; }
        public string ConnectionString { get; set; }
    }
}
