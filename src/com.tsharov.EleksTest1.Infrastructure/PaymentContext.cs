﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using com.tsharov.EleksTest1.Infrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace com.tsharov.EleksTest1.Infrastructure
{
    public class PaymentContext : DbContext
    {
        public const string DEFAULT_SCHEMA = "Payments";

        public DbSet<CurrencyCode> CurrencyCodes { get; set; }
        public DbSet<TransactionStatus> TransactionStatuses { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public PaymentContext() : base()
        {
        }

        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CurrencyCodeEntityTypeConfiguration());
            builder.ApplyConfiguration(new TransactionStatusEntityTypeConfiguration());
            builder.ApplyConfiguration(new TransactionEntityTypeConfiguration());
            builder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
        }

    }

    public class PaymentContextDesignFactory : IDesignTimeDbContextFactory<PaymentContext>
    {
        public PaymentContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<PaymentContext>()
                .UseSqlServer("Server=.;Initial Catalog=PaymentDb;Integrated Security=true");

            return new PaymentContext(optionsBuilder.Options);
        }
    }
}
