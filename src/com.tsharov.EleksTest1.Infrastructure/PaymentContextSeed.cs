﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.Infrastructure
{
    public class PaymentContextSeed
    {
        public async Task SeedAsync(PaymentContext context, IHostingEnvironment env, IOptions<PaymentSettings> settings, ILogger<PaymentContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(PaymentContextSeed));

            await policy.ExecuteAsync(async () =>
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                string contentRootPath = env.ContentRootPath;

                using (context)
                {
                    Console.WriteLine("SeedAsync 1");
                    context.Database.Migrate();
                    Console.WriteLine("SeedAsync 2");
                    if (!context.CurrencyCodes.Any())
                    {
                        Console.WriteLine("SeedAsync 3");
                        context.CurrencyCodes.AddRange(useCustomizationData
                                                ? GetCurrencyCodesFromFile(contentRootPath, logger)
                                                : GetPredefinedCurrencyCodes());

                        await context.SaveChangesAsync();
                    }
                    Console.WriteLine("SeedAsync 4");
                    if (!context.TransactionStatuses.Any())
                    {
                        context.TransactionStatuses.AddRange(useCustomizationData
                                                ? GetTransactionStatusFromFile(contentRootPath, logger)
                                                : GetPredefinedTransactionStatus());
                        await context.SaveChangesAsync();
                    }

                    if (!context.Customers.Any())
                    {
                        context.Customers.AddRange(useCustomizationData
                                                ? GetCustomersFromFile(contentRootPath, logger)
                                                : GetPredefinedCustomers());
                        await context.SaveChangesAsync();
                    }
                }

            });
        }

        private IEnumerable<CurrencyCode> GetCurrencyCodesFromFile(string contentRootPath, ILogger<PaymentContextSeed> log)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<CurrencyCode> GetPredefinedCurrencyCodes()
        {
            return Enumeration.GetAll<CurrencyCode>();
        }

        private IEnumerable<TransactionStatus> GetTransactionStatusFromFile(string contentRootPath, ILogger<PaymentContextSeed> log)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<TransactionStatus> GetPredefinedTransactionStatus()
        {
            return Enumeration.GetAll<TransactionStatus>();
        }

        private IEnumerable<Customer> GetCustomersFromFile(string contentRootPath, ILogger<PaymentContextSeed> log)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<Customer> GetPredefinedCustomers()
        {
            return new List<Customer>
            {
                new Customer
                {
                    Id = 123456,
                    Name = "Firstname Lastname",
                    Email = "user@domain.com",
                    Mobile = "0123456789",
                    Transactions = new List<Transaction>
                    {
                        new Transaction
                        {
                            Id = 1,
                            Time = new DateTime(2018, 2, 28, 21, 34, 0),
                            Amount = 1234.56M,
                            CurrencyCode = CurrencyCode.USD,
                            Status = TransactionStatus.Success
                        },
                        new Transaction
                        {
                            Id = 2,
                            Time = new DateTime(2018, 11, 1, 8, 34, 0),
                            Amount = 0.56M,
                            CurrencyCode = CurrencyCode.THB,
                            Status = TransactionStatus.Failed
                        }
                    }
                 }
            };
        }

        private Policy CreatePolicy(ILogger<PaymentContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().
                WaitAndRetryAsync(
                    retryCount: retries,
                    sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                    onRetry: (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogWarning(exception, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries}", prefix, exception.GetType().Name, exception.Message, retry, retries);
                    }
                );
        }
    }
}
