﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace com.tsharov.EleksTest1.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Payments");

            migrationBuilder.CreateSequence(
                name: "customerseq",
                schema: "Payments",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "transactionseq",
                schema: "Payments",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "CurrencyCodes",
                schema: "Payments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false, defaultValue: 1),
                    Name = table.Column<string>(maxLength: 3, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                schema: "Payments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    Email = table.Column<string>(maxLength: 25, nullable: false),
                    Mobile = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransactionStatuses",
                schema: "Payments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false, defaultValue: 1),
                    Name = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                schema: "Payments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false),
                    CustomerId = table.Column<long>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    CurrencyCodeId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_CurrencyCodes_CurrencyCodeId",
                        column: x => x.CurrencyCodeId,
                        principalSchema: "Payments",
                        principalTable: "CurrencyCodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transactions_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalSchema: "Payments",
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transactions_TransactionStatuses_StatusId",
                        column: x => x.StatusId,
                        principalSchema: "Payments",
                        principalTable: "TransactionStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CurrencyCodeId",
                schema: "Payments",
                table: "Transactions",
                column: "CurrencyCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CustomerId",
                schema: "Payments",
                table: "Transactions",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_StatusId",
                schema: "Payments",
                table: "Transactions",
                column: "StatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions",
                schema: "Payments");

            migrationBuilder.DropTable(
                name: "CurrencyCodes",
                schema: "Payments");

            migrationBuilder.DropTable(
                name: "Customers",
                schema: "Payments");

            migrationBuilder.DropTable(
                name: "TransactionStatuses",
                schema: "Payments");

            migrationBuilder.DropSequence(
                name: "customerseq",
                schema: "Payments");

            migrationBuilder.DropSequence(
                name: "transactionseq",
                schema: "Payments");
        }
    }
}
