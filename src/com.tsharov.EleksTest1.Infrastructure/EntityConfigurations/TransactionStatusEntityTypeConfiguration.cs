﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.Infrastructure.EntityConfigurations
{
    class TransactionStatusEntityTypeConfiguration : IEntityTypeConfiguration<TransactionStatus>
    {
        public void Configure(EntityTypeBuilder<TransactionStatus> transactionStatusesConfiguration)
        {
            transactionStatusesConfiguration.ToTable("TransactionStatuses", PaymentContext.DEFAULT_SCHEMA);

            transactionStatusesConfiguration.HasKey(ct => ct.Id);

            transactionStatusesConfiguration.Property(ct => ct.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            transactionStatusesConfiguration.Property(ct => ct.Name)
                .HasMaxLength(10)
                .IsRequired();
        }
    }
}