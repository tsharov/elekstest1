﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.Infrastructure.EntityConfigurations
{
    class CustomerEntityTypeConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> customerConfiguration)
        {
            customerConfiguration.ToTable("Customers", PaymentContext.DEFAULT_SCHEMA);

            customerConfiguration.HasKey(c => c.Id);

            customerConfiguration.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("customerseq", PaymentContext.DEFAULT_SCHEMA);

            customerConfiguration.Property<string>("Name")
                .HasMaxLength(30)
                .IsRequired();

            customerConfiguration.Property<string>("Email")
                .HasMaxLength(25)
                .IsRequired();

            customerConfiguration.Property<string>("Mobile")
                .HasMaxLength(10)
                .IsRequired(false);

            customerConfiguration.HasMany(c => c.Transactions)
                .WithOne(r => r.Customer)
                .HasForeignKey("CustomerId")
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
