﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.Infrastructure.EntityConfigurations
{
    class CurrencyCodeEntityTypeConfiguration : IEntityTypeConfiguration<CurrencyCode>
    {
        public void Configure(EntityTypeBuilder<CurrencyCode> currencyCodesConfiguration)
        {
            currencyCodesConfiguration.ToTable("CurrencyCodes", PaymentContext.DEFAULT_SCHEMA);

            currencyCodesConfiguration.HasKey(ct => ct.Id);

            currencyCodesConfiguration.Property(ct => ct.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            currencyCodesConfiguration.Property(ct => ct.Name)
                .HasMaxLength(3)
                .IsRequired();
        }
    }

}
