﻿using com.tsharov.EleksTest1.BusinessLayer.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.Infrastructure.EntityConfigurations
{
    class TransactionEntityTypeConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> transactionConfiguration)
        {
            transactionConfiguration.ToTable("Transactions", PaymentContext.DEFAULT_SCHEMA);

            transactionConfiguration.HasKey(t => t.Id);

            transactionConfiguration.Property(t => t.Id)
                .ForSqlServerUseSequenceHiLo("transactionseq", PaymentContext.DEFAULT_SCHEMA);

            transactionConfiguration.Property<DateTime>("Time").IsRequired();
            transactionConfiguration.Property<decimal>("Amount").IsRequired();

            transactionConfiguration.HasOne(t => t.CurrencyCode)
                .WithMany()
                .HasForeignKey("CurrencyCodeId")
                .IsRequired();

            transactionConfiguration.HasOne(t => t.Status)
                .WithMany()
                .HasForeignKey("StatusId")
                .IsRequired();
        }
    }

}
