﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.tsharov.EleksTest1.Infrastructure.Exceptions
{
    /// <summary>
    /// Exception type for app exceptions
    /// </summary>
    public class CustomerDomainException : Exception
    {
        public CustomerDomainException()
        { }

        public CustomerDomainException(string message)
            : base(message)
        { }

        public CustomerDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
